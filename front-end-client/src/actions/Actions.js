import APIService from '../services/APIService';
import * as ActionConstants from '../constants/ActionConstants';
import Response from '../services/Response';

export function exchangePairsFetchSuccess(pairs) {
  return {
    type: ActionConstants.EXCHANGE_PAIRS_FETCH_SUCCESS,
    pairs,
  };
}

export function setLoadingState(isLoading) {
  return {
    type: ActionConstants.EXCHANGE_PAIRS_ARE_LOADING,
    isLoading,
  };
}

export function setCurrencyBase(base) {
  return {
    type: ActionConstants.SET_CURRENCY_BASE,
    base,
  };
}

export function exchangePairsFetchFailure(didFetchFail) {
  return {
    type: ActionConstants.EXCHANGE_PAIRS_FETCH_FAILURE,
    didFetchFail,
  };
}

export function getExchangePairs(currencyBase) {
  return async (dispatch) => {
    try {
      const result = await APIService.getExchangePairs(currencyBase);
      const response = new Response(result);

      if (!response.isSuccessful()) {
        throw new Error('API error');
      }

      dispatch(exchangePairsFetchSuccess(response.getData()));
    } catch (err) {
      dispatch(exchangePairsFetchFailure(true));
    }
  };
}

export function setFilterTerm(filterTerm) {
  return {
    type: ActionConstants.SET_FILTER_TERM,
    filterTerm,
  };
}
