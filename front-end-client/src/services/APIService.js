import axios from 'axios';
import Urls from './Urls';

class APIService {
  getExchangePairs(currencyBase) {
    return axios.get(Urls.getExchangePairsUrl(currencyBase));
  }
}

export default new APIService();
