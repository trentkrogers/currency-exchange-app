export default class Urls {
  static getRootAPIUrl() {
    return 'http://localhost:3000/api';
  }

  static getExchangePairsUrl(currencyBase) {
    return `${this.getRootAPIUrl()}/exchange-rates?base=${currencyBase}`;
  }
}
