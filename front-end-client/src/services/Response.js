export default class Response {
  constructor(res) {
    this.data = res.data;
    this.status = res.status;
    this.statusText = res.statusText;
    this.headers = res.headers;
    this.config = res.config;
  }

  getData() {
    return this.data;
  }

  getStatusCode() {
    return this.status;
  }

  isSuccessful() {
    const statusCode = this.getStatusCode();
    return statusCode > 199 && statusCode < 300;
  }
}
