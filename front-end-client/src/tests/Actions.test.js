import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as Actions from '../actions/Actions';
import * as ActionConstants from '../constants/ActionConstants';
import mockAxios from './__mocks__/axios';

jest.mock('axios');

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('actions', () => {
  it('should create an action on fetch exchange pairs success', () => {
    const response = {
      AUD: 1.3656,
      BGN: 1.6813,
      BRL: 4.0923,
      CAD: 1.3051,
    };

    mockAxios.get.mockImplementation(() => {
      return Promise.resolve({ data: response, status: 200 });
    });

    const expectedAction = [
      {
        type: ActionConstants.EXCHANGE_PAIRS_FETCH_SUCCESS,
        pairs: response,
      },
    ];

    const store = mockStore({ exchangePairs: response });

    store.dispatch(Actions.getExchangePairs()).then(() => {
      expect(store.getActions()).toEqual(expectedAction);
    });
  });

  it('should create an action on fetch exchange pairs failure', () => {
    const didFetchFail = true;

    mockAxios.get.mockImplementation(() => {
      return Promise.resolve({ data: {}, status: 400 });
    });

    const expectedAction = [
      {
        type: ActionConstants.EXCHANGE_PAIRS_FETCH_FAILURE,
        didFetchFail,
      },
    ];

    const store = mockStore();

    store.dispatch(Actions.getExchangePairs()).then(() => {
      expect(store.getActions()).toEqual(expectedAction);
    });
  });

  it('should create an action to update filter term', () => {
    const filterTerm = 'test';

    const expectedAction = {
      type: ActionConstants.SET_FILTER_TERM,
      filterTerm,
    };

    expect(Actions.setFilterTerm(filterTerm)).toEqual(expectedAction);
  });

  it('should create an action to set the loading state', () => {
    const isLoading = false;

    const expectedAction = {
      type: ActionConstants.EXCHANGE_PAIRS_ARE_LOADING,
      isLoading,
    };

    expect(Actions.setLoadingState(isLoading)).toEqual(expectedAction);
  });

  it('should create an action to set the currency base', () => {
    const base = 'JPY';

    const expectedAction = {
      type: ActionConstants.SET_CURRENCY_BASE,
      base,
    };

    expect(Actions.setCurrencyBase(base)).toEqual(expectedAction);
  });
});
