import * as Actions from '../actions/Actions';
import * as Reducer from '../reducers/Reducers';

describe('Reducers', () => {
  it('should handle EXCHANGE_PAIRS_FETCH_SUCCESS', () => {
    const initialState = {};

    const AUD = 'AUD';
    const AUDValue = 1.3656;

    const pairs = {
      [AUD]: AUDValue,
      BGN: 1.6813,
      BRL: 4.0923,
      CAD: 1.3051,
    };

    const action = Actions.exchangePairsFetchSuccess(pairs);

    const newState = Reducer.exchangePairs(initialState, action);

    expect(Object.keys(newState)[0]).toEqual(AUD);
    expect(Object.values(newState)[0]).toEqual(AUDValue);
    expect(Object.keys(newState).length).toEqual(4);
  });

  it('should handle EXCHANGE_PAIRS_FETCH_FAILURE', () => {
    const initialState = false;

    const didFetchFail = true;

    const action = Actions.exchangePairsFetchFailure(didFetchFail);

    const newState = Reducer.exchangePairsFetchFailed(initialState, action);

    expect(newState).toEqual(didFetchFail);
  });

  it('should handle SET_FILTER_TERM', () => {
    const initialState = '';

    const filterTerm = 'testFilterTerm'.toUpperCase();

    const action = Actions.setFilterTerm(filterTerm);

    const newState = Reducer.filterTerm(initialState, action);

    expect(newState).toEqual(filterTerm);
  });

  it('should handle EXCHANGE_PAIRS_ARE_LOADING', () => {
    const initialState = true;

    const isLoading = false;

    const action = Actions.setLoadingState(isLoading);

    const newState = Reducer.exchangePairsAreLoading(initialState, action);

    expect(newState).toEqual(isLoading);
  });

  it('should handle SET_CURRENCY_BASE', () => {
    const initialState = 'USD';

    const currencyBase = 'CAD';

    const action = Actions.setCurrencyBase(currencyBase);

    const newState = Reducer.currencyBase(initialState, action);

    expect(newState).toEqual(currencyBase);
  });
});
