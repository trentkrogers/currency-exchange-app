import React from 'react';
import renderer from 'react-test-renderer';
import FilterBox from '../../components/main/filterBox/FilterBox';

test('FilterBox renders correctly', () => {
  const component = renderer.create(<FilterBox />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
