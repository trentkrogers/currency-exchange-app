import React from 'react';
import renderer from 'react-test-renderer';
import ExchangePairs from '../../components/main/exchangePairs/ExchangePairs';

const props = {
  pairs: {
    AUD: 1.3656,
    BGN: 1.6813,
    BRL: 4.0923,
    CAD: 1.3051,
  },
  fetchFailed: false,
  isLoading: false,
};

test('ExchangePairs renders correctly', () => {
  const component = renderer.create(<ExchangePairs {...props} />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
