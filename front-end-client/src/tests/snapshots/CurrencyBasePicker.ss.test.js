import React from 'react';
import renderer from 'react-test-renderer';
import CurrencyBasePicker from '../../components/main/currencyBasePicker/CurrencyBasePicker';

const props = {
  currencyBases: [
    'USD',
    'AUD',
    'BGN',
    'BRL',
    'CAD',
    'CHF',
    'CNY',
    'CZK',
    'DKK',
    'EUR',
    'GBP',
    'HKD',
    'HRK',
    'HUF',
    'IDR',
    'ILS',
    'INR',
    'ISK',
    'JPY',
    'KRW',
    'MXN',
    'MYR',
    'NOK',
    'NZD',
    'PHP',
    'PLN',
    'RON',
    'RUB',
    'SEK',
    'SGD',
    'THB',
    'TRY',
    'ZAR',
  ],
};

test('CurrencyBasePicker renders correctly', () => {
  const component = renderer.create(<CurrencyBasePicker {...props} />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
