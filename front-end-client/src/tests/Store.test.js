import { createStore } from 'redux';
import { reducers } from '../reducers/CombinedReducers';
import * as Actions from '../actions/Actions';

describe('Store', () => {
  let initialState;
  let store;

  beforeEach(() => {
    initialState = {};
    store = createStore(reducers, initialState);
  });

  it('should handle setting exchangePairs', () => {
    const pairs = {
      AUD: 1.3656,
      BGN: 1.6813,
      BRL: 4.0923,
      CAD: 1.3051,
    };

    const action = Actions.exchangePairsFetchSuccess(pairs);

    store.dispatch(action);

    const actual = store.getState().exchangePairs;

    expect(actual).toEqual(pairs);
  });

  it('should handle setting exchangePairsFetchFailed', () => {
    const didFetchFail = true;

    const action = Actions.exchangePairsFetchFailure(didFetchFail);

    store.dispatch(action);

    const actual = store.getState().exchangePairsFetchFailed;

    expect(actual).toEqual(didFetchFail);
  });

  it('should handle setting filter term', () => {
    const filterTerm = 'testyMcTestFace'.toUpperCase();

    const action = Actions.setFilterTerm(filterTerm);

    store.dispatch(action);

    const actual = store.getState().filterTerm;

    expect(actual).toEqual(filterTerm);
  });

  it('should handle setting loading state', () => {
    const isLoading = false;

    const action = Actions.setLoadingState(isLoading);

    store.dispatch(action);

    const actual = store.getState().exchangePairsAreLoading;

    expect(actual).toEqual(isLoading);
  });

  it('should handle setting currency base', () => {
    const currencyBase = 'SEK';

    const action = Actions.setCurrencyBase(currencyBase);

    store.dispatch(action);

    const actual = store.getState().currencyBase;

    expect(actual).toEqual(currencyBase);
  });
});
