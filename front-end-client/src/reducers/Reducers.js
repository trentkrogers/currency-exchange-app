import * as ActionConstants from '../constants/ActionConstants';

export function exchangePairs(state = {}, action) {
  switch (action.type) {
    case ActionConstants.EXCHANGE_PAIRS_FETCH_SUCCESS:
      return action.pairs;

    default:
      return state;
  }
}

export function filterTerm(state = '', action) {
  switch (action.type) {
    case ActionConstants.SET_FILTER_TERM:
      return action.filterTerm.toUpperCase();

    default:
      return state;
  }
}

export function exchangePairsFetchFailed(state = false, action) {
  switch (action.type) {
    case ActionConstants.EXCHANGE_PAIRS_FETCH_FAILURE:
      return action.didFetchFail;

    default:
      return state;
  }
}

export function exchangePairsAreLoading(state = true, action) {
  switch (action.type) {
    case ActionConstants.EXCHANGE_PAIRS_ARE_LOADING:
      return action.isLoading;

    default:
      return state;
  }
}

export function currencyBase(state = 'USD', action) {
  switch (action.type) {
    case ActionConstants.SET_CURRENCY_BASE:
      return action.base;

    default:
      return state;
  }
}
