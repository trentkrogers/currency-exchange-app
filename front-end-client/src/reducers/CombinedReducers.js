import { combineReducers } from 'redux';
import {
  exchangePairs, exchangePairsFetchFailed, exchangePairsAreLoading, filterTerm, currencyBase,
} from './Reducers';

export const reducers = combineReducers({
  exchangePairs,
  exchangePairsFetchFailed,
  exchangePairsAreLoading,
  filterTerm,
  currencyBase,
});
