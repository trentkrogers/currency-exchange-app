import React from 'react';

const Header = (p) => {
  return (
    <section className="hero is-info">
      <div className="hero-body">
        <div className="container">
          <h1 className="title">
            {p.currencyBase}
            {' '}
Exchange Rates
          </h1>
        </div>
      </div>
    </section>
  );
};

export default Header;
