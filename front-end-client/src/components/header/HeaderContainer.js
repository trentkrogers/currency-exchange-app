import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Header from './Header';

class HeaderContainer extends Component {
  render() {
    const props = {
      currencyBase: this.props.currencyBase,
    };

    return <Header {...props} />;
  }
}

const mapStateToProps = (state) => {
  return {
    currencyBase: state.currencyBase,
  };
};

HeaderContainer.propTypes = {
  currencyBase: PropTypes.string.isRequired,
};

export default connect(mapStateToProps)(HeaderContainer);
