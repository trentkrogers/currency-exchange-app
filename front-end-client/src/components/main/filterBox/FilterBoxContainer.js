import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { setFilterTerm } from '../../../actions/Actions';
import FilterBox from './FilterBox';

class FilterBoxContainer extends Component {
  handleChange = (e) => {
    const filterTerm = e.target.value;
    this.props.setFilterTerm(filterTerm);
  };

  render() {
    const props = {
      handleChange: this.handleChange,
    };

    return <FilterBox {...props} />;
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setFilterTerm: filterTerm => dispatch(setFilterTerm(filterTerm)),
  };
};

FilterBoxContainer.propTypes = {
  setFilterTerm: PropTypes.func.isRequired,
};

export default connect(
  null,
  mapDispatchToProps,
)(FilterBoxContainer);
