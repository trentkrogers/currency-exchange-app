import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getExchangePairs, setLoadingState, setCurrencyBase } from '../../../actions/Actions';
import CurrencyBasePicker from './CurrencyBasePicker';

class CurrencyBasePickerContainer extends Component {
  constructor(props) {
    super(props);

    this.currencyBases = [
      'USD',
      'AUD',
      'BGN',
      'BRL',
      'CAD',
      'CHF',
      'CNY',
      'CZK',
      'DKK',
      'EUR',
      'GBP',
      'HKD',
      'HRK',
      'HUF',
      'IDR',
      'ILS',
      'INR',
      'ISK',
      'JPY',
      'KRW',
      'MXN',
      'MYR',
      'NOK',
      'NZD',
      'PHP',
      'PLN',
      'RON',
      'RUB',
      'SEK',
      'SGD',
      'THB',
      'TRY',
      'ZAR',
    ];
  }

  handleClick = async (e) => {
    const selectedCurrencyBase = e.target.id;

    this.props.setLoadingState(true);
    this.props.setCurrencyBase(selectedCurrencyBase);
    await this.props.getExchangePairs(selectedCurrencyBase);
    this.props.setLoadingState(false);
  };

  render() {
    const props = {
      handleClick: this.handleClick,
      currencyBases: this.currencyBases,
    };

    return <CurrencyBasePicker {...props} />;
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getExchangePairs: base => dispatch(getExchangePairs(base)),
    setLoadingState: isLoading => dispatch(setLoadingState(isLoading)),
    setCurrencyBase: base => dispatch(setCurrencyBase(base)),
  };
};

CurrencyBasePickerContainer.propTypes = {
  getExchangePairs: PropTypes.func.isRequired,
  setLoadingState: PropTypes.func.isRequired,
  setCurrencyBase: PropTypes.func.isRequired,
};

export default connect(
  null,
  mapDispatchToProps,
)(CurrencyBasePickerContainer);
