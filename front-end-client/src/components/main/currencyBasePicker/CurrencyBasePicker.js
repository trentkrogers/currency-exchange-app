import React from 'react';

const CurrencyBasePicker = (p) => {
  return (
    <div>
      <label className="label">Select a currency base</label>
      <div className="dropdown is-hoverable">
        <div className="dropdown-trigger">
          <button className="button" aria-haspopup="true" aria-controls="dropdown-menu">
            <span>Bases</span>
          </button>
        </div>
        <div className="dropdown-menu" id="dropdown-menu" role="menu">
          <div className="dropdown-content">
            {p.currencyBases.map((base) => {
              return (
                <a key={base} onClick={p.handleClick} href id={base} className="dropdown-item">
                  {base}
                </a>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default CurrencyBasePicker;
