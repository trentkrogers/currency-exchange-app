import React from 'react';
import CurrencyBasePickerContainer from './currencyBasePicker/CurrencyBasePickerContainer';
import ExchangePairsContainer from './exchangePairs/ExchangePairsContainer';
import FilterBoxContainer from './filterBox/FilterBoxContainer';

const Main = () => {
  return (
    <main className="site-content">
      <div className="section">
        <div className="container">
          <div className="columns is-centered">
            <div className="column is-one-quarter">
              <CurrencyBasePickerContainer />
            </div>
            <div className="column is-one-quarter">
              <FilterBoxContainer />
            </div>
          </div>
          <ExchangePairsContainer />
        </div>
      </div>
    </main>
  );
};

export default Main;
