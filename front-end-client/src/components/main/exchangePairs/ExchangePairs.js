import React from 'react';
import loading from '../../../assets/loading.gif';

const ExchangePairs = (p) => {
  const noResultsFound = () => {
    return Object.keys(p.pairs).length === 0;
  };

  if (p.isLoading) {
    return (
      <div className="columns is-centered">
        <img src={loading} alt="loading..." />
      </div>
    );
  }
  if (p.fetchFailed) {
    return <p className="is-size-4">Could not load currency pair data. Please try reloading the page.</p>;
  }
  if (noResultsFound()) {
    return <p className="is-size-4">No results found :(</p>;
  }
  return (
    <div className="columns is-multiline is-centered">
      {Object.keys(p.pairs).map((pair) => {
        return (
          <div key={pair} className="column is-one-quarter">
            <div className="card">
              <div className="card-content has-text-centered">
                <p className="title">{pair}</p>
                <p className="subtitle">{p.pairs[pair]}</p>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default ExchangePairs;
