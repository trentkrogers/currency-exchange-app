import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ExchangePairs from './ExchangePairs';
import { getExchangePairs, setLoadingState } from '../../../actions/Actions';

class ExchangePairsContainer extends Component {
  async componentDidMount() {
    const defaultBase = 'USD';
    await this.props.getExchangePairs(defaultBase);
    this.props.setLoadingState(false);
  }

  render() {
    const props = {
      pairs: this.props.exchangePairs,
      fetchFailed: this.props.fetchFailed,
      isLoading: this.props.exchangePairsAreLoading,
    };
    return <ExchangePairs {...props} />;
  }
}

const filterAndSortAlphabetically = (filterTerm, unordered) => {
  const ordered = {};

  Object.keys(unordered)
    .filter(key => key.includes(filterTerm.toUpperCase()))
    .sort()
    .forEach((key) => {
      ordered[key] = unordered[key];
    });

  return ordered;
};

const mapStateToProps = (state) => {
  return {
    exchangePairs: filterAndSortAlphabetically(state.filterTerm, state.exchangePairs),
    exchangePairsAreLoading: state.exchangePairsAreLoading,
    fetchFailed: state.exchangePairsFetchFailed,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getExchangePairs: currencyBase => dispatch(getExchangePairs(currencyBase)),
    setLoadingState: isLoading => dispatch(setLoadingState(isLoading)),
  };
};

ExchangePairsContainer.propTypes = {
  exchangePairs: PropTypes.object.isRequired,
  exchangePairsAreLoading: PropTypes.bool.isRequired,
  fetchFailed: PropTypes.bool.isRequired,
  getExchangePairs: PropTypes.func.isRequired,
  setLoadingState: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ExchangePairsContainer);
