import React from 'react';
import './App.css';
import 'bulma/css/bulma.css';
import HeaderContainer from './header/HeaderContainer';
import Main from './main/Main';
import Footer from './footer/Footer';

const App = () => {
  return (
    <div className="site">
      <HeaderContainer />
      <Main />
      <Footer />
    </div>
  );
};

export default App;
