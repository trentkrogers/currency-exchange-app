const server = require("../server");
const chai = require("chai");
const should = chai.should();
const chaiHttp = require("chai-http");
chai.use(chaiHttp);

describe("GET /api/exchange-rates", () => {
  it("should GET all rates", done => {
    chai
      .request(server)
      .get("/api/exchange-rates?base=USD")
      .end((err, res) => {
        should.not.exist(err);
        res.status.should.equal(200);
        res.type.should.equal("application/json");
        res.body.should.include.keys("AUD", "JPY", "DKK");
        done();
      });
  });
});
