const express = require("express");
const axios = require("axios");

const app = express();
const port = process.env.PORT || 5000;

app.get("/api/exchange-rates", (req, res) => {
  const base = req.query.base;
  axios.get(`https://api.exchangeratesapi.io/latest?base=${base}`).then(result => {
    res.send(result.data.rates);
  });
});

app.listen(port, () => console.log(`Listening on port ${port}`));

module.exports = app;
