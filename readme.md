# Exchange Rates App

A simple and responsive app that displays current exchange rates of a selected currency.

## Getting Started

After cloning the repository:

```
cd currency-exchange-app
npm i && cd front-end-client && npm i && cd ..
npm run start # concurrently starts the client and the server
```

## Running Tests

API test:

```
npm run test-server # run in currency-exchange-app folder to test the api
```

Front-end client tests:

```
npm run test-client # run in front-end-client folder to test the client
```

## Built With

- [React](https://reactjs.org/)
- [Redux](https://redux.js.org/)
- [Bulma](https://bulma.io/)
- [Node.js](https://nodejs.org/en/)
- [Express](https://expressjs.com/)
